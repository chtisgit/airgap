#include <array>

#include <cassert>
#include <cstdint>
#include <cstdio>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "sendreceiver/Simple2Bit.h"
#include "tone.h"
#include "transcoder/Interleaver.h"

class Mapping {
	union {
		uint8_t *octet;
		void *original = MAP_FAILED;
	} buf;
	size_t sz;
	int fd = -1;

public:
	Mapping(const char *filename)
	{
		struct stat st;

		if (stat(filename, &st) != 0)
			throw std::runtime_error("cannot stat " +
			                         std::string(filename));
		sz = st.st_size;

		fd = open(filename, O_RDONLY);
		if (fd == -1)
			throw std::runtime_error("error opening file " +
			                         std::string(filename));

		buf.original = mmap(nullptr, sz, PROT_READ, MAP_PRIVATE, fd, 0);
		if (buf.original == MAP_FAILED) {
			close(fd);
			throw std::runtime_error("cannot mmap");
		}
	}

	~Mapping()
	{
		if (fd != -1)
			close(fd);
		if (buf.original != MAP_FAILED)
			munmap(buf.original, sz);
	}

	const uint8_t *data() const
	{
		return buf.octet;
	}

	size_t size() const
	{
		return sz;
	}
};

int main(int argc, char **argv)
{
	if (argc < 2) {
		printf("error: no file given!\n\n");
		return 1;
	}
	Mapping datafile(argv[1]);
	ToneProvider tonepr;
	Simple2Bit sr(&tonepr);
	Interleaver I(4, 2);

	sr.addTranscoder(&I);
	sr.send(datafile.data(), datafile.size());

	printf("\n");
	return 0;
}