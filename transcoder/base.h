#pragma once

#include <cstdint>
#include <cstdlib>
#include <utility>

class Transcoder {
public:
	Transcoder();
	virtual ~Transcoder();

	virtual size_t encodedSize(size_t len, uint8_t *data = nullptr);
	virtual size_t decodedSize(size_t len, uint8_t *data = nullptr);
	virtual size_t encode(uint8_t *data, size_t len, size_t capacity);
	virtual size_t decode(uint8_t *data, size_t len, size_t capacity);
};
