#include "Interleaver.h"

#include <gtest/gtest.h>
#include <vector>

TEST(Interleaver, TestSingleByte)
{
	constexpr int N = 2, M = 4;
	constexpr uint8_t INIT = 0x5A;

	uint8_t byte = INIT;

	auto copy = byte;
	ASSERT_TRUE(byte == copy);

	Interleaver il1(N, M);

	il1.encode(&byte, 1, 1);
	EXPECT_EQ(0x3C, byte);

	il1.decode(&byte, 1, 1);
	EXPECT_EQ(INIT, byte);
}

TEST(Interleaver, TestN6M8)
{
	constexpr int N = 6, M = 8;
	std::array<uint8_t, 18> testdata{43,  210, 137, 8,  101, 77,
	                                 67,  19,  0,   94, 244, 50,
	                                 170, 200, 99,  36, 145, 223};

	static_assert(testdata.size() % (N * M / 8) == 0,
	              "currently required by Interleaver");

	auto copy = testdata;
	ASSERT_TRUE(testdata == copy);

	Interleaver il1(N, M);

	EXPECT_EQ(il1.encodedSize(copy.size()), testdata.size());

	il1.encode(copy.data(), copy.size(), copy.size());

	EXPECT_EQ(il1.decodedSize(copy.size()), testdata.size());

	il1.decode(copy.data(), copy.size(), copy.size());

	EXPECT_TRUE(testdata == copy);
}

TEST(Interleaver, TestEqualNM)
{
	constexpr int N = 8, M = 8;
	std::array<uint8_t, 16> testdata{43, 210, 137, 8,  101, 77,  67, 19,
	                                 0,  94,  244, 50, 170, 200, 99, 36};

	static_assert(testdata.size() % (N * M / 8) == 0,
	              "currently required by Interleaver");

	auto copy = testdata;
	Interleaver il1(N, M);

	ASSERT_TRUE(testdata == copy);

	EXPECT_EQ(il1.encodedSize(copy.size()), testdata.size());

	il1.encode(copy.data(), copy.size(), copy.size());

	EXPECT_EQ(il1.decodedSize(copy.size()), testdata.size());

	il1.decode(copy.data(), copy.size(), copy.size());

	EXPECT_TRUE(testdata == copy);
}

TEST(Interleaver, TestLargeFuzzed)
{
	constexpr int N = 32, M = 18, LEN = N * M * 1000;

	std::vector<uint8_t> testdata(LEN);
	for (auto &val : testdata) {
		val = rand() % 256;
	}

	auto copy = testdata;
	Interleaver il1(N, M);

	ASSERT_TRUE(testdata == copy);

	EXPECT_EQ(il1.encodedSize(copy.size()), testdata.size());

	il1.encode(copy.data(), copy.size(), copy.size());

	EXPECT_EQ(il1.decodedSize(copy.size()), testdata.size());

	il1.decode(copy.data(), copy.size(), copy.size());

	EXPECT_TRUE(testdata == copy);
}