#include "Interleaver.h"

#include <algorithm>
#include <stdexcept>

Interleaver::Interleaver(size_t n, size_t m) : buf(n * m), N(n), M(m)
{
	if ((n * m) % 8 != 0) {
		throw std::invalid_argument("n*m must be a multiple of 8");
	}
}

Interleaver::Interleaver(size_t n) : Interleaver(n, n)
{
}

Interleaver::~Interleaver()
{
}

size_t Interleaver::encodedSize(size_t len, uint8_t *data)
{
	return len;
}
size_t Interleaver::decodedSize(size_t len, uint8_t *data)
{
	return len;
}

static uint8_t getBit(uint8_t *data, size_t byte, size_t bit)
{
	return (data[byte] >> bit) & 1;
}
static void toggleBit(uint8_t *data, size_t byte, size_t bit)
{
	data[byte] ^= (1 << bit);
}

template <bool encode>
size_t Interleaver::code(uint8_t *data, size_t len, size_t capacity)
{
	const size_t step = N * M / 8;
	const auto *start = data;
	for (; size_t(data - start) < len; data += step) {
		std::copy(data, data + step, buf.begin());
		for (size_t i = 0; i < N; i++) {
			for (size_t j = 0; j < M; j++) {
				size_t srcbyte, srcbit;
				size_t dstbyte, dstbit;

				if (encode) {
					// copy bit j*N+i to position i*M+j
					srcbyte = (j * N + i) / 8;
					srcbit = (j * N + i) % 8;
					dstbyte = (i * M + j) / 8;
					dstbit = (i * M + j) % 8;
				} else { // !encode ergo decode
					srcbyte = (i * M + j) / 8;
					srcbit = (i * M + j) % 8;
					dstbyte = (j * N + i) / 8;
					dstbit = (j * N + i) % 8;
				}
				if (getBit(buf.data(), srcbyte, srcbit) !=
				    getBit(data, dstbyte, dstbit)) {
					toggleBit(data, dstbyte, dstbit);
				}
			}
		}
	}
	return len;
}

size_t Interleaver::encode(uint8_t *data, size_t len, size_t capacity)
{
	return code<true>(data, len, capacity);
}
size_t Interleaver::decode(uint8_t *data, size_t len, size_t capacity)
{
	return code<false>(data, len, capacity);
}

#ifdef TESTS
#include "Interleaver_test.cpp"
#endif
