#pragma once

#include "base.h"

#include <utility>
#include <vector>

#include <cstdint>
#include <cstdlib>

class Interleaver : public Transcoder {
	std::vector<uint8_t> buf;

	template<bool encode>
	size_t code(uint8_t *data, size_t len, size_t capacity);
public:
	const size_t N, M;

	Interleaver(size_t n, size_t m);
	Interleaver(size_t n);
	virtual ~Interleaver();

	virtual size_t encodedSize(size_t len, uint8_t *data = nullptr);
	virtual size_t decodedSize(size_t len, uint8_t *data = nullptr);
	virtual size_t encode(uint8_t *data, size_t len, size_t capacity);
	virtual size_t decode(uint8_t *data, size_t len, size_t capacity);
};
