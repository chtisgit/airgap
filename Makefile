
RM ?= rm
CXX ?= clang

EXTOBJS = tone.o  sendreceiver/base.o  sendreceiver/Simple2Bit.o transcoder/base.o transcoder/Interleaver.o
OBJS := $(EXTOBJS) airgap.o
LIBS = -lsfml-audio -lsfml-system

all: airgap

test: CFLAGS += -DTESTS -pthread
test: LIBS += libgtest.a libgtest_main.a
test: clean airgap-test clean
	./airgap-test

airgap: $(OBJS)
	$(CXX) $(LFLAGS) -o $@ $(OBJS) $(LIBS)

airgap-test: $(EXTOBJS)
	$(CXX) $(CFLAGS) -o $@ $(EXTOBJS) $(LIBS)

%.o: %.cpp
	$(CXX) -O2 -g -Wall $(CFLAGS) $(CXXFLAGS) -std=c++11 -c -o $@ $<

clean:
	$(RM) *.o airgap sendreceiver/*.o transcoder/*.o

.PHONY: all clean test