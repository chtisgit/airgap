#pragma once

#include <memory>

#include <SFML/Audio.hpp>

class Tone {
	sf::SoundBuffer buffer;
	sf::Sound sound;

public:
	const double frequency;

	Tone(double freq);
	void play();
	void pause();
	void stop();
};

class ToneProvider {
	std::vector<std::weak_ptr<Tone>> tones;
	double accuracy = 0.005;

public:
	static constexpr int MAX = 100;

	double getAccuracy()
	{
		return accuracy;
	}
	void setAccuracy(double a)
	{
		accuracy = a;
	}

	std::shared_ptr<Tone> get(double freq);
	static bool compare(double f1, double f2, double acc);
};
