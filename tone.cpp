#include "tone.h"

#include <cmath>

Tone::Tone(double freq) : frequency(freq)
{
	constexpr double SAMPLE_RATE = 44100;
	constexpr size_t SAMPLES = SAMPLE_RATE;

	sf::Int16 raw[SAMPLES];

	const unsigned AMPLITUDE = 30000;
	const double TWO_PI = 6.28318;
	const double increment = freq / SAMPLE_RATE;
	double x = 0;
	for (size_t i = 0; i < SAMPLES; i++) {
		raw[i] = AMPLITUDE * sin(x * TWO_PI);
		x = increment * i;
	}

	if (!buffer.loadFromSamples(raw, SAMPLES, 1, SAMPLE_RATE)) {
		throw std::runtime_error("could not load sound buffer!");
	}
	sound.setBuffer(buffer);
	sound.setLoop(true);
}

void Tone::play()
{
	sound.play();
}

void Tone::pause()
{
	sound.pause();
}

void Tone::stop()
{
	sound.stop();
}

std::shared_ptr<Tone> ToneProvider::get(double freq)
{
	for (auto it = tones.begin(); it != tones.end();) {
		auto tone = it->lock();
		if (tone && compare(tone->frequency, freq, accuracy)) {
			return tone;
		}
		if (!tone) {
			++it;
			tones.erase(it);
		} else {
			++it;
		}
	}
	if (tones.size() + 1 > MAX)
		return nullptr;

	auto tone = std::make_shared<Tone>(freq);
	tones.emplace_back(tone);
	return tone;
}


bool ToneProvider::compare(double f1, double f2, double accuracy) {
	return f1*(1-accuracy) <= f2 && f1*(1+accuracy) >= f2;
}