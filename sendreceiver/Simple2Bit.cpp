#include "Simple2Bit.h"

#include <cassert>
#include <stdexcept>

#include <SFML/Audio.hpp>

// static const std::vector<double> FREQUENCIES{18000, 18500, 19000, 19500};
// static const std::vector<double> FREQUENCIES{15000, 16000, 17000, 18000};
static const std::vector<double> FREQUENCIES{1046.5, 1568, 2637, 4698};

Simple2Bit::Simple2Bit(ToneProvider *tp)
{
	for(auto freq : FREQUENCIES){
		auto tone = tp->get(freq);
		if(!tone)
			throw std::runtime_error("Simple2Bit: cannot create tone!");
		tones.push_back(tone);
	}
}
Simple2Bit::~Simple2Bit()
{
}

void Simple2Bit::sendChunk(const uint8_t *data, size_t len)
{
	tones[0]->play();
	sf::sleep(sf::milliseconds(1000));

	for (size_t i = 0; i < len; i++) {
		auto cache = data[i];
		for (auto i = 8; i > 0; i -= 2) {
			auto select = (cache & 3);
			assert(select >= 0 && select < 4);

			tones[select]->play();
			sf::sleep(sf::milliseconds(100));
			tones[select]->stop();
			cache >>= 2;
		}
	}
}
void Simple2Bit::recvChunk(uint8_t *data, size_t len)
{
	throw std::runtime_error("not implemented");
}