#include "base.h"

#include <algorithm>
#include <stdexcept>

SendReceiver::SendReceiver()
{
}
SendReceiver::~SendReceiver()
{
}

void SendReceiver::applyTranscoder(Transcoder *t, bool encode)
{
	auto len = buffer.size();
	auto needSize = encode ? t->encodedSize(len, buffer.data())
	                       : t->decodedSize(len, buffer.data());
	if (needSize > buffer.size()) {
		buffer.resize(needSize);
	}

	if (encode)
		len = t->encode(buffer.data(), buffer.size(), buffer.size());
	else
		len = t->decode(buffer.data(), buffer.size(), buffer.size());

	buffer.resize(len);
}

void SendReceiver::addTranscoder(Transcoder *tc)
{
	transcoders.push_back(tc);
}

void SendReceiver::send(const uint8_t *data, size_t len)
{
	// TODO ensure that buffer is cleared at the end of the function
	if (transcoders.empty()) {
		sendChunk(data, len);
	} else {
		buffer.assign(data, data + len);
		for (Transcoder *t : transcoders) {
			applyTranscoder(t, true);
		}
		sendChunk(buffer.data(), buffer.size());
		buffer.clear();
	}
}
void SendReceiver::recv(uint8_t *data, size_t len, size_t capacity)
{
	// TODO ensure that buffer is cleared at the end of the function
	if (!buffer.empty()) {
		// if the last call failed copy the decoded data that remained
		// in the internal buffer to data if possible
		if (buffer.size() > capacity) {
			throw std::out_of_range("capacity too small");
		}
		std::copy(buffer.cbegin(), buffer.cend(), data);
		buffer.clear();
		return;
	}
	recvChunk(data, len);

	if (transcoders.empty()) {
		return;
	}

	buffer.assign(data, data + len);
	for (auto it = transcoders.rbegin(); it != transcoders.rend(); ++it) {
		applyTranscoder(*it, false);
	}
	if (buffer.size() > capacity) {
		throw std::out_of_range("capacity too small");
	}
	std::copy(buffer.cbegin(), buffer.cend(), data);
	buffer.clear();
}
void SendReceiver::sendChunk(const uint8_t *data, size_t len)
{
}
void SendReceiver::recvChunk(uint8_t *data, size_t len)
{
}
