#pragma once

#include "../tone.h"
#include "../transcoder/base.h"

#include <cstdint>
#include <cstdlib>
#include <vector>

class SendReceiver {
	std::vector<Transcoder *> transcoders;
	std::vector<uint8_t> buffer;

	void applyTranscoder(Transcoder *t, bool encode);

public:
	SendReceiver();
	virtual ~SendReceiver();

	void addTranscoder(Transcoder *tc);
	void send(const uint8_t *data, size_t len);
	void recv(uint8_t *data, size_t len, size_t capacity);

	virtual void sendChunk(const uint8_t *data, size_t len);
	virtual void recvChunk(uint8_t *data, size_t len);
};
