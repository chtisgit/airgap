#pragma once

#include "../tone.h"
#include "base.h"

#include <cstdint>
#include <cstdlib>

class Simple2Bit : public SendReceiver {
	std::vector<std::shared_ptr<Tone>> tones;

public:
	Simple2Bit(ToneProvider *tp);
	virtual ~Simple2Bit();

	virtual void sendChunk(const uint8_t *data, size_t len);
	virtual void recvChunk(uint8_t *data, size_t len);
};
